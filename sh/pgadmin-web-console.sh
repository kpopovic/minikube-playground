#!/bin/bash

NS_COUNT=$(minikube kubectl -- get namespaces | grep -c minikube-playground) || echo "Namespace minikube-playground not found."

if [[ $NS_COUNT -eq 1 ]]; then
    SERVICE_NAME=$(minikube kubectl -- get svc -n minikube-playground | grep -oiE "pgadmin.{0,20}[[:space:]]" | cut -d ' ' -f1 | xargs)

    echo -e '\n'
    echo "pgadmin web login form - email: $(minikube kubectl -- get pods -o=jsonpath='{.items[*].spec.containers[*].env[?(@.name=="PGADMIN_DEFAULT_EMAIL")].value}' -n minikube-playground)"
    echo "pgadmin web login form - password: $(minikube kubectl -- get secret pgadmin-credentials --template=\{\{.data.password}} -n minikube-playground | base64 --decode)"
    echo '-------------------------------------------'
    echo "pgadmin web - postgres connection IP: $(minikube ip)"
    echo "pgadmin web - postgres username: $(minikube kubectl -- get secret postgres-credentials --template=\{\{.data.username}} -n minikube-playground | base64 --decode)"
    echo "pgadmin web - postgres password: $(minikube kubectl -- get secret postgres-credentials --template=\{\{.data.password}} -n minikube-playground | base64 --decode)"

    minikube service "$SERVICE_NAME" -n minikube-playground
fi
