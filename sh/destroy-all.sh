#!/bin/bash

NS_COUNT=$(minikube kubectl -- get namespaces | grep -c minikube-playground) || echo "Namespace minikube-playground not found."

if [[ $NS_COUNT -eq 1 ]]; then
    minikube kubectl -- delete deployments --all -n minikube-playground
    minikube kubectl -- delete services --all -n minikube-playground
    minikube kubectl -- delete pods --all -n minikube-playground
    minikube kubectl -- delete pvc --all -n minikube-playground
    minikube kubectl -- delete pv --all -n minikube-playground
    minikube kubectl -- delete secret postgres-credentials -n minikube-playground
    minikube kubectl -- delete secret pgadmin-credentials -n minikube-playground
    minikube kubectl -- delete namespace minikube-playground
fi
