#!/bin/bash

ROOT_DIR=$(cd .. && pwd)
POSTGRES_POD_DIR="$ROOT_DIR/postgres-pod"
PGADMIN_POD_DIR="$ROOT_DIR/pgadmin-pod"

NS_COUNT=$(minikube kubectl -- get namespaces | grep -c minikube-playground)

if [[ $NS_COUNT -eq 0 ]]; then
    minikube kubectl -- create namespace minikube-playground
fi

minikube kubectl -- apply -f "$POSTGRES_POD_DIR"/postgres-secret.yaml
minikube kubectl -- apply -f "$POSTGRES_POD_DIR"/postgres-pv.yaml
minikube kubectl -- apply -f "$POSTGRES_POD_DIR"/postgres-pv-claim.yaml
minikube kubectl -- apply -f "$POSTGRES_POD_DIR"/postgres-service.yaml
minikube kubectl -- apply -f "$POSTGRES_POD_DIR"/postgres-deployment.yaml

minikube kubectl -- apply -f "$PGADMIN_POD_DIR"/pgadmin-secret.yaml
minikube kubectl -- apply -f "$PGADMIN_POD_DIR"/pgadmin-service.yaml
minikube kubectl -- apply -f "$PGADMIN_POD_DIR"/pgadmin-deployment.yaml
