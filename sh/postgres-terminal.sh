#!/bin/bash

NS_COUNT=$(minikube kubectl -- get namespaces | grep -c minikube-playground) || echo "Namespace minikube-playground not found."

if [[ $NS_COUNT -eq 1 ]]; then
    SERVICE_NAME=$(minikube kubectl -- get svc -n minikube-playground | grep -oiE "postgres.{0,20}[[:space:]]" | xargs)
    minikube kubectl -- exec -ti service/"$SERVICE_NAME" -n minikube-playground -- psql -U postgres
fi
