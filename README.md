# minikube-playground

## Description
Purpose of this repo is to make simple minikube (https://minikube.sigs.k8s.io/docs/) POD samples with basic and well known software components.

## Minikube installation
Read the manual here. https://minikube.sigs.k8s.io/docs/start/

## Clone git project
git clone https://gitlab.com/kpopovic/minikube-playground.git

## Useful commands
- deploy-all.sh - deploys all pods to minikube
- destroy-all.sh - destroys all running pods in minikube
- pgadmin-web-console.sh - will show pgadmin web page (browser) where user can connect to postgres server
- postgres-terminal.sh - postgres shell terminal (no web ui)

## Roadmap
- [x] postgres 16.x pod
- [x] pgadmin 4.x pod
- [ ] SpringBoot x.y REST API pod, CRUD (Create-Read-Update-Delete) operations with postgres database

## Authors and acknowledgment
Krešimir Popović, Senior Software Developer at ENEA (Telecom & Cybersecurity software company)

## License
EUROPEAN UNION PUBLIC LICENCE v. 1.2
